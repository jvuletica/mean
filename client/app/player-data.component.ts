import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Player } from "./player"

@Component({
  selector: 'player-data',
  templateUrl: "app/player-data.component.html",
})
export class PlayerDataComponent {
    viewState = "readOnly"

    instantiatePlayer(): void {
        this.player = {
            _id: "",
            id: "",
            uniqueId: "",
            firstName: "",
            lastName: "",
            dateOfBirth: "",
            placeOfBirth: "",
            country: ""
        }
    }

    @Input()
    player: Player

    @Output()
    onNew = new EventEmitter<Player>();
    new(player: Player) {
        this.instantiatePlayer()
        this.viewState = "new"
    }

    @Output()
    onEdit = new EventEmitter<Player>();
    edit(player: Player) {
        this.viewState = "edit"
        this.onEdit.emit(player)
    }

    @Output()
    onSave = new EventEmitter<Player>();
    save(player: Player) {
        this.viewState === "new" ? this.onNew.emit(player) : this.onSave.emit(player)
        this.viewState = "new"
    }

    @Output()
    onDelete = new EventEmitter<Player>();
    delete(player: Player) {
        this.viewState = "search"
        this.onDelete.emit(player)
        this.viewState = "edit"
    }

    @Output()
    onSearch = new EventEmitter<Player>();
    search(player: Player) {
        if(!this.player) this.instantiatePlayer()
        else this.onSearch.emit(player)
        this.viewState = "search"
    }

    @Output()
    onGetAll = new EventEmitter<Player>();
    getAll() {this.onGetAll.emit()}
}
