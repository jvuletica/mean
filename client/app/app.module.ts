import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }  from './app.component';
import { HttpModule }    from '@angular/http';
import { PlayerDataComponent } from './player-data.component';
import { PlayerRegistrationComponent } from './player-registration.component';
import { PlayerSearchComponent } from './player-search.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    PlayerDataComponent,
    PlayerRegistrationComponent,
    PlayerSearchComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
