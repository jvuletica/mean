import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Player } from "./player"

@Component({
  selector: 'player-search',
  templateUrl: "app/player-search.component.html",
})
export class PlayerSearchComponent {
    @Input()
    players: Player[]

    @Output()
    onViewPlayer = new EventEmitter<Player>();
    viewPlayer(player: Player) {this.onViewPlayer.emit(player)}
}
