"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var player_1 = require("./player");
var PlayerDataComponent = (function () {
    function PlayerDataComponent() {
        this.viewState = "readOnly";
        this.onNew = new core_1.EventEmitter();
        this.onEdit = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
        this.onSearch = new core_1.EventEmitter();
        this.onGetAll = new core_1.EventEmitter();
    }
    PlayerDataComponent.prototype.instantiatePlayer = function () {
        this.player = {
            _id: "",
            id: "",
            uniqueId: "",
            firstName: "",
            lastName: "",
            dateOfBirth: "",
            placeOfBirth: "",
            country: ""
        };
    };
    PlayerDataComponent.prototype.new = function (player) {
        this.instantiatePlayer();
        this.viewState = "new";
    };
    PlayerDataComponent.prototype.edit = function (player) {
        this.viewState = "edit";
        this.onEdit.emit(player);
    };
    PlayerDataComponent.prototype.save = function (player) {
        this.viewState === "new" ? this.onNew.emit(player) : this.onSave.emit(player);
        this.viewState = "new";
    };
    PlayerDataComponent.prototype.delete = function (player) {
        this.viewState = "search";
        this.onDelete.emit(player);
        this.viewState = "edit";
    };
    PlayerDataComponent.prototype.search = function (player) {
        if (!this.player)
            this.instantiatePlayer();
        else
            this.onSearch.emit(player);
        this.viewState = "search";
    };
    PlayerDataComponent.prototype.getAll = function () { this.onGetAll.emit(); };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', player_1.Player)
    ], PlayerDataComponent.prototype, "player", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerDataComponent.prototype, "onNew", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerDataComponent.prototype, "onEdit", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerDataComponent.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerDataComponent.prototype, "onDelete", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerDataComponent.prototype, "onSearch", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerDataComponent.prototype, "onGetAll", void 0);
    PlayerDataComponent = __decorate([
        core_1.Component({
            selector: 'player-data',
            templateUrl: "app/player-data.component.html",
        }), 
        __metadata('design:paramtypes', [])
    ], PlayerDataComponent);
    return PlayerDataComponent;
}());
exports.PlayerDataComponent = PlayerDataComponent;
//# sourceMappingURL=player-data.component.js.map