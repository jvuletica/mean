import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Player } from "./player"
import { Club } from "./club"

@Injectable()
export class PlayerService {
    constructor(private http: Http) { }

    private headers = new Headers({'Content-Type': 'application/json'});
    private serverUrl = "http://localhost:3005/api"

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error)
        return Promise.reject(error.message || error)
    }

    getAll(): Promise<Player[]> {
        return this.http.get(this.serverUrl)
        .toPromise()
        .then(response => response.json() as Player[])
        .catch(this.handleError);
    }

    create(player: Player): Promise<Player> {
        return this.http.post(this.serverUrl, JSON.stringify(player), {headers: this.headers})
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError)
    }

    update(player: Player): Promise<Player> {
        const url = `${this.serverUrl}/${player._id}`;
        return this.http
        .put(url, JSON.stringify(player), {headers: this.headers})
        .toPromise()
        .then(() => player)
        .catch(this.handleError);
    }

    delete(id: string): Promise<void> {
        const url = `${this.serverUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
        .toPromise()
        .then(() => null)
        .catch(this.handleError);
    }

    search(player: Player): Promise<Player[]>{
        var playerString = JSON.stringify(player)
        const url = `${this.serverUrl}/find/?search=${playerString}`
        return this.http.get(url)
        .toPromise()
        .then(response => response.json() as Player[])
        .catch(this.handleError)
    }

    addClub(player: Player, club: Club): Promise<Club> {
        var combinedObj = {
            club: club,
            player: player
        }
        return this.http.post(this.serverUrl + "/club", JSON.stringify(combinedObj), {headers: this.headers})
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError)
    }

}
