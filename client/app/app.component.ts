import { Component } from '@angular/core';
import { PlayerService } from './player.service';
import { OnInit } from '@angular/core';
import { Player } from "./player"
import { Club } from "./club"

@Component({
  selector: 'app',
  templateUrl: "app/app.component.html",
  styles: [`.selected{color: red}`],
  providers: [PlayerService]
})


export class AppComponent implements OnInit{
  constructor(private playerService: PlayerService) { }
  ngOnInit(): void{this.playerService.getAll().then(players => {this.players = players; console.log("players", this.players)})}

  players: Player[]
  selectedPlayer: Player;

  refresh() {
    this.playerService.getAll().then(players => {this.players = players})
  }

  onSearch(player: Player)
  {
    console.log("onSearch", player)
    this.playerService.search(player).then(foundPlayers => this.players = foundPlayers)
  }

  onNew(player: Player)
  {
    console.log("onNew", player)
    this.playerService.create(player).then(() => console.log("player created"))
  }

  onEdit(player: Player)
  {
    console.log("onEdit", player)
  }

  onSave(player: Player)
  {
    console.log("onSave", player)
    this.playerService.update(player).then(() => this.refresh())
  }

  onDelete(player: Player)
  {
    console.log("onDelete", player)
    this.playerService.delete(player._id).then(() => this.refresh())
    this.refresh()
  }

  onViewPlayer(player: Player) {this.selectedPlayer = player}
  onGetAll(){this.refresh()}
  onAddNewClub(club: Club){this.playerService.addClub(this.selectedPlayer, club).then(() => console.log("club added"))}

}

