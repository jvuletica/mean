export class Player {
    _id: string;
    id: string;
    uniqueId: string;
    firstName: string;
    lastName: string;
    dateOfBirth: string;
    placeOfBirth: string;
    country: string;
}