"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PlayerSearchComponent = (function () {
    function PlayerSearchComponent() {
        this.onViewPlayer = new core_1.EventEmitter();
    }
    PlayerSearchComponent.prototype.viewPlayer = function (player) { this.onViewPlayer.emit(player); };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], PlayerSearchComponent.prototype, "players", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerSearchComponent.prototype, "onViewPlayer", void 0);
    PlayerSearchComponent = __decorate([
        core_1.Component({
            selector: 'player-search',
            templateUrl: "app/player-search.component.html",
        }), 
        __metadata('design:paramtypes', [])
    ], PlayerSearchComponent);
    return PlayerSearchComponent;
}());
exports.PlayerSearchComponent = PlayerSearchComponent;
//# sourceMappingURL=player-search.component.js.map