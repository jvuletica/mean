"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var player_service_1 = require('./player.service');
var AppComponent = (function () {
    function AppComponent(playerService) {
        this.playerService = playerService;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.playerService.getAll().then(function (players) { _this.players = players; console.log("players", _this.players); });
    };
    AppComponent.prototype.refresh = function () {
        var _this = this;
        this.playerService.getAll().then(function (players) { _this.players = players; });
    };
    AppComponent.prototype.onSearch = function (player) {
        var _this = this;
        console.log("onSearch", player);
        this.playerService.search(player).then(function (foundPlayers) { return _this.players = foundPlayers; });
    };
    AppComponent.prototype.onNew = function (player) {
        console.log("onNew", player);
        this.playerService.create(player).then(function () { return console.log("player created"); });
    };
    AppComponent.prototype.onEdit = function (player) {
        console.log("onEdit", player);
    };
    AppComponent.prototype.onSave = function (player) {
        var _this = this;
        console.log("onSave", player);
        this.playerService.update(player).then(function () { return _this.refresh(); });
    };
    AppComponent.prototype.onDelete = function (player) {
        var _this = this;
        console.log("onDelete", player);
        this.playerService.delete(player._id).then(function () { return _this.refresh(); });
        this.refresh();
    };
    AppComponent.prototype.onViewPlayer = function (player) { this.selectedPlayer = player; };
    AppComponent.prototype.onGetAll = function () { this.refresh(); };
    AppComponent.prototype.onAddNewClub = function (club) { this.playerService.addClub(this.selectedPlayer, club).then(function () { return console.log("club added"); }); };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            templateUrl: "app/app.component.html",
            styles: [".selected{color: red}"],
            providers: [player_service_1.PlayerService]
        }), 
        __metadata('design:paramtypes', [player_service_1.PlayerService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map