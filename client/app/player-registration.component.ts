import {  Component, EventEmitter, Input, Output } from '@angular/core';
import { Club } from "./club"

@Component({
  selector: 'player-registration',
  templateUrl: "app/player-registration.component.html",
})
export class PlayerRegistrationComponent {
    clubs: Club[]
    club: Club

    instantiateClub() {
        this.club = {
            name: "",
            dateFrom: "",
            dateTo: ""
        }
        if(!this.clubs) this.clubs = []
        this.clubs.push(this.club)
    }

    addNewRegistration() {this.instantiateClub()}
    @Output()
    onAddNewClub = new EventEmitter<Club>();
    addNewClub(club: Club) {
        this.onAddNewClub.emit(club)
    }

    @Output()
    onDeleteClub = new EventEmitter<Club>();
    deleteClub(club: Club) {
        this.onDeleteClub.emit(club)
    }
}
