"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PlayerRegistrationComponent = (function () {
    function PlayerRegistrationComponent() {
        this.onAddNewClub = new core_1.EventEmitter();
        this.onDeleteClub = new core_1.EventEmitter();
    }
    PlayerRegistrationComponent.prototype.instantiateClub = function () {
        this.club = {
            name: "",
            dateFrom: "",
            dateTo: ""
        };
        if (!this.clubs)
            this.clubs = [];
        this.clubs.push(this.club);
    };
    PlayerRegistrationComponent.prototype.addNewRegistration = function () { this.instantiateClub(); };
    PlayerRegistrationComponent.prototype.addNewClub = function (club) {
        this.onAddNewClub.emit(club);
    };
    PlayerRegistrationComponent.prototype.deleteClub = function (club) {
        this.onDeleteClub.emit(club);
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerRegistrationComponent.prototype, "onAddNewClub", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PlayerRegistrationComponent.prototype, "onDeleteClub", void 0);
    PlayerRegistrationComponent = __decorate([
        core_1.Component({
            selector: 'player-registration',
            templateUrl: "app/player-registration.component.html",
        }), 
        __metadata('design:paramtypes', [])
    ], PlayerRegistrationComponent);
    return PlayerRegistrationComponent;
}());
exports.PlayerRegistrationComponent = PlayerRegistrationComponent;
//# sourceMappingURL=player-registration.component.js.map