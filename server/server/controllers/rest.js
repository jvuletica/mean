var mongoose = require("mongoose")
Club = mongoose.model("Club")
Player = mongoose.model("Player")

exports.findAll = function(req, res)
{
  Player.find({}, function(err, players)
  {
    if(err) console.log(err)
    res.send(players)
  })
}
exports.search = function(req, res)
{
  var player = JSON.parse(req.query.search)
  Player.find({$or: [
    {"uniqueId": player.uniqueId},
    {"firstName": player.firstName},
    {"lastName": player.lastName}]},
    function(err, players)
    {
      if(err) console.log(err)
      res.send(players)
    })
}

exports.add = function(req, res)
{
  delete req.body._id
  var player = new Player(req.body)

  player.save(function(err, player)
  {
    if(err) console.log(err)
    res.send(player)
  })
}

exports.update = function(req, res)
{
  delete req.body._id
  Player.findByIdAndUpdate(req.params.id, {$set: req.body}, function(err, player)
  {
    if (err) console.log(err);
    res.send(player);
  })
}

exports.remove = function(req, res)
{
  Player.findByIdAndRemove(req.params.id, function(err, player)
  {
    if (err) console.log(err);
    res.send(player);
  })
}

exports.addClub = function(req, res)
{
  console.log("GLEDAJ", req.body.club, req.body.player)
  delete req.body._id
  // var club = JSON.parse(req.body.club)
  // var player = JSON.parse(req.body.player)
  // player.club.push(club)
  // console.log("GLEDAJ",player.club);

  // Player.findByIdAndUpdate(req.params.id, {$set: {club: club.id}}, function(err, player)
  // {
  // })
  //
  // club.save(function(err, club)
  // {
  //   if(err) console.log(err)
  //   res.send(club)
  // })
}
