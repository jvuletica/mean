var rest = require("../server/controllers/rest")

module.exports = (app) =>
{
  app.get("/", (req, res) => {console.log("root GET"); res.send({test: "radi"})})
  app.get("/api", rest.findAll)
  app.get("/api/find", rest.search)
  app.post("/api", rest.add)
  app.put("/api/:id", rest.update)
  app.post("/api/club", rest.addClub)
  app.delete("/api/:id", rest.remove)
}
